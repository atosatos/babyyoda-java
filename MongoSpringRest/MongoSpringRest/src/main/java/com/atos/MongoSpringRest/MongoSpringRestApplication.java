package com.atos.MongoSpringRest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MongoSpringRestApplication {

	public static void main(String[] args) {
		SpringApplication.run(MongoSpringRestApplication.class, args);
	}

}
