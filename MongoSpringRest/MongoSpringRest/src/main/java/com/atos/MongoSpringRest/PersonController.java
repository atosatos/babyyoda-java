package com.atos.MongoSpringRest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
public class PersonController {
    @Autowired
    PersonRepository pr;
    @CrossOrigin(origins = "*")
    @GetMapping(path = "/getPersons", produces = "application/json")
    public List<PersonEntity> getAll(){
        try {
            List<PersonEntity> list = new ArrayList<>();
                pr.findAll().forEach(list::add);

            if (list.isEmpty()) {
                return null;
            }

            return list;
        } catch (Exception e) {

            return null;
        }

    }
    @CrossOrigin(origins = "*")
    @GetMapping(path="/get",produces = "application/json")
    public String get(){
        return "Hello0";
    }
    @CrossOrigin(origins = "*")
    @GetMapping("/")
    public String welcome(){
        return "welcome";
    }
    @CrossOrigin(origins = "*")
    @PostMapping("/addPerson")
    public String saveEmployee(@RequestBody PersonEntity emp) {
        pr.save(emp);
        return "employee added successfully::"+emp.getFirstName();

    }
}
