package com.atos.MongoSpringRest;

import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "items")
public class PersonEntity {
    private String FirstName;
    private String LastName;
    private String Adress;
    private String City;
   public PersonEntity(){}
    public PersonEntity(String fName,String lName,String adress,String city){
       this.FirstName=fName;
       this.LastName=lName;
       this.Adress=adress;
       this.City=city;
    }
    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String firstName) {
        FirstName = firstName;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String lastName) {
        LastName = lastName;
    }

    public String getAdress() {
        return Adress;
    }

    public void setAdress(String adress) {
        Adress = adress;
    }

    public String getCity() {
        return City;
    }

    public void setCity(String city) {
        City = city;
    }
}
