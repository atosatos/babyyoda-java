package com.atos.MongoSpringRest;

import org.springframework.data.mongodb.repository.MongoRepository;


public interface PersonRepository extends MongoRepository<PersonEntity, String> {

}
